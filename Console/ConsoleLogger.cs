﻿using System;
using System.Linq;
using Domain;
using Newtonsoft.Json;

namespace ConsoleRunner
{
    public class ConsoleLogger : ILogger
    {
        private readonly JsonSerializerSettings _jsonSettings;

        public ConsoleLogger()
        {
            _jsonSettings = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, Formatting = Formatting.Indented };
        }

        public void Log(params object[] arg)
        {
            Log(LogLevel.Normal, "{0}", arg);
        }

        public void Log(LogLevel level, params object[] arg)
        {
            Log(level, "{0}", arg);
        }

        public void Log(string format, params object[] arg)
        {
            Log(LogLevel.Normal, format, arg);
        }

        public void Log(LogLevel level, string format, params object[] arg)
        {
            var args = arg.Select(val =>
                val == null
                ? null
                : (val.GetType().Namespace.StartsWith("System")
                    ? val
                    : JsonConvert.SerializeObject(val, _jsonSettings)))
                .ToArray();

            switch (level)
            {
                case LogLevel.Normal:
                    Console.ResetColor();
                    break;
                case LogLevel.Debug:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case LogLevel.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case LogLevel.Trace:
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    break;
            }
            
            Console.WriteLine(format, args);
            
            Console.ResetColor();
        }
    }
}
