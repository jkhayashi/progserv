﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using progserv.Impl;
using progserv.Impl.Data;
using progserv.Impl.Programs;
using progserv.Impl.Repos;
using progserv.Impl.Services;

namespace ConsoleRunner
{
    public class Program
    {
        private static readonly Random Random = new Random();

        private readonly ILogger _logger;
        private readonly ProgramRunner _runner;
        private readonly InMemoryProgramRepository _programRepo;

        private Program()
        {
            _logger = new ConsoleLogger();

            var serviceReg = new InMemoryServiceRegistry();
            serviceReg.RegisterService("sys.customer", new Customer { Id = "123", Name = "Seattle Counseling" });
            serviceReg.RegisterService("sys.person_id_gen", new PersonIdService());
            serviceReg.RegisterService("sys.db", new InMemoryObjectRepository(_logger));

            _programRepo = new InMemoryProgramRepository();
            _programRepo.Save("patient.list", new ListCustomerPatients());
            _programRepo.Save("patient.new", new NewPatientProgram());
            _programRepo.Save("patient.assessment", new PatientAssessmentProgram());
            _programRepo.Save("patient.find", new FindPatientProgram());

            _runner = new ProgramRunner(serviceReg, _programRepo, new InMemoryProcessRepository(), new ProcessIdGenerator());
        }

        void Run()
        {
            Console.CancelKeyPress += (e, o) => Environment.Exit(0);

            const string defaultProgram = "patient.new";

            var programs = _programRepo
                .ListPrograms()
                .Select((program, index) => new { program, index })
                .ToDictionary(item => item.index + 1, item => item.program);

            do
            {
                _logger.Log("Programs:");
                foreach (var program in programs)
                {
                    _logger.Log("   {0}. {1}", program.Key, program.Value);
                }
                _logger.Log("Program to run [{0}]: ", defaultProgram);

                var line = Console.ReadLine();
                if (line == null) continue;
                var programName = string.IsNullOrEmpty(line) ? defaultProgram : line.Trim();

                int progId;
                if (int.TryParse(programName, out progId))
                    programName = programs[progId];

                var inputs = new Dictionary<string, object>();

                _logger.Log("Executing program {0}", programName);

                var result = _runner.Start(programName);

                while (result is SuspendedResult)
                {
                    var suspendedResult = (SuspendedResult)result;
                    _logger.Log(LogLevel.Debug, "Suspended result:\n{0}", result);

                    inputs.Clear();

                    foreach (var task in suspendedResult.Tasks)
                    {
                        foreach (var error in task.Errors ?? new string[0])
                        {
                            _logger.Log(LogLevel.Error, error);
                        }

                        foreach (var input in task.Inputs)
                        {
                            string defaultValue = null;
                            Dictionary<int, string> candidatePrograms = new Dictionary<int, string>();

                            switch (input.Type)
                            {
                                case "text":
                                    defaultValue =
                                        string.Join("",
                                            Enumerable.Range(0, 8)
                                                .Select(e => (char)Random.Next('a', 'z')));
                                    break;
                                case "date":
                                    defaultValue = string.Format("{0}-{1}-{2}", Random.Next(1950, 1990),
                                        Random.Next(1, 12), Random.Next(1, 28));
                                    break;
                                case "choice":
                                    var choiceInput = (ChoiceInput)input;
                                    foreach (var choice in choiceInput.Choices)
                                        Console.WriteLine("({0}) {1}", choice.Value, choice.Label);
                                    break;
                                default:
                                    _logger.Log("Don't know how to ask for `{0}`. Here are programs that can provide one:",
                                        input.Type);
                                    candidatePrograms = _programRepo
                                        .ListPrograms(input.Type)
                                        .Select((program, index) => new { program, index })
                                        .ToDictionary(item => item.index + 1, item => item.program);

                                    foreach (var program in candidatePrograms)
                                    {
                                        _logger.Log("   {0}. {1}", program.Key, program.Value);
                                    }
                                    break;
                            }

                            _logger.Log("{0}{1}{2}> ",
                                input.Label != null ? input.Label + ", " : "",
                                input.Type,
                                defaultValue != null ? " [" + defaultValue + "]" : null);

                            var val = Console.ReadLine().Trim();
                            val = string.IsNullOrEmpty(val) ? defaultValue : val;

                            object typedVal;
                            switch (input.Type)
                            {
                                case "text":
                                    typedVal = val;
                                    break;
                                case "date":
                                    typedVal = DateTime.Parse(val);
                                    break;
                                case "choice":
                                    typedVal = val;
                                    break;
                                default:

                                    int candidateProgId;

                                    var candidateProgName = int.TryParse(val, out candidateProgId)
                                        ? candidatePrograms[candidateProgId]
                                        : val;

                                    typedVal = new RunProgramRequest { ProgramName = candidateProgName };
                                    break;
                            }

                            inputs.Add(input.Name, typedVal);
                        }
                    }

                    result = _runner.Resume(result.ProcessId, inputs);
                }
                _logger.Log(LogLevel.Debug, "Complete result:\n{0}", result);
            } while (true);
        }

        static void Main(string[] args)
        {
            new Program().Run();
        }
    }
}
