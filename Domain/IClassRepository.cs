﻿namespace Domain
{
    public interface IClassRepository
    {
        void ValidateClassNames(string[] classNames);
    }
}
