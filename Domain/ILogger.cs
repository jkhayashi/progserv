﻿namespace Domain
{
    public enum LogLevel
    {
        Trace,
        Debug,
        Normal,
        Error
    }

    public interface ILogger
    {
        void Log(LogLevel level, params object[] arg);
        void Log(LogLevel level, string format, params object[] arg);
        void Log(params object[] arg);
        void Log(string format, params object[] arg);
    }
}
