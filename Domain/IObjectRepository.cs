﻿using System.Collections.Generic;

namespace Domain
{
    public interface IObjectRepository
    {
        void Save(string type, string key, object obj);
        void SaveRelation(string fromType, string fromKey, IEnumerable<string> relations, string toType, string toKey);
        bool HasRelation(string fromType, string fromKey, string relation, string toType, string toKey);
        object[] GetRelated(string fromType, string fromKey, string relation, string toType); 
        object Get(string type, string key);
        object[] Find(string type, IDictionary<string, object> properties);
    }
}
