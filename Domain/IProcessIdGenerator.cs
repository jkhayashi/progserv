﻿namespace Domain
{
    public interface IProcessIdGenerator
    {
        string GenerateId();
    }
}
