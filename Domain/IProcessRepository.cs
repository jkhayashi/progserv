﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;

namespace Domain
{
    public class Process
    {
        public readonly string ProcessId;
        public Stack<ProcessStackItem> Stack { get; set; }

        public Process(string processId, IProgram program)
        {
            ProcessId = processId;

            Stack = new Stack<ProcessStackItem>();
            Stack.Push(new ProcessStackItem(new ExpandoObject(), program));
        }

        public ProcessResult Resume(IServiceRegistry serviceRegistry, IDictionary<string, object> inputs = null, IProgram withProgram = null)
        {
            ProcessResult lastCompleteResult;

            if (withProgram != null)
                Stack.Push(new ProcessStackItem(new ExpandoObject(), withProgram));

            do
            {
                // Run latest program on the stack
                var progItem = Stack.Pop();

                var program = progItem.Program;
                var heap = progItem.Heap;

                var dependencies = serviceRegistry.GetServices(program.Dependencies.Select(d => d.Name).ToArray());
                program.Init(heap, dependencies);
                var result = program.Execute(inputs);

                var completeResult = result as CompletedResult;
                if (completeResult != null)
                {
                    inputs = completeResult.Output;
                    lastCompleteResult = completeResult;
                }
                else
                {
                    // Program not yet complete; put back onto the stack
                    Stack.Push(progItem);
                    return result;
                }
            } while (Stack.Any());

            return lastCompleteResult;
        }
    }

    public class ProcessStackItem
    {
        public dynamic Heap { get; set; }
        public IProgram Program { get; set; }

        public ProcessStackItem(dynamic heap, IProgram program)
        {
            Heap = heap;
            Program = program;
        }
    }

    public interface IProcessRepository
    {
        IEnumerable<ProcessResult> List();
        Process Get(string processId);
        void Save(string processId, Process process, ProcessResult result);
        void Delete(string processId);
    }
}
