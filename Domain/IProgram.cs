﻿using System.Collections.Generic;

namespace Domain
{
    public interface IProgram
    {
        // List of program dependencies
        Dependency[] Dependencies { get; }
        Output[] Outputs { get; }
        void Init(dynamic heap, IDictionary<string, object> dependencies);
        ProcessResult Execute(IDictionary<string, object> inputs);
        void Abort();
    }

    public class Input
    {
        public string Name { get; set; }
        public string Label { get; set; }
        public string Type { get; set; }
        public string UiTypeHint { get; set; }
    }

    public class ChoiceInput : Input
    {
        public Choice[] Choices { get; set; }

        public ChoiceInput()
        {
            Type = "choice";
        }
    }

    public class Choice
    {
        public string Label { get; set; }
        public object Value { get; set; }
    }

    public class Output
    {
        public string Name { get; set; }
    }

    public abstract class ProcessResult
    {
        public string ProcessId { get; set; }
    }

    public class SuspendedResult : ProcessResult
    {
        public class SuspendedTask
        {
            public string Name { get; set; }
            public string[] Errors { get; set; }
            public Input[] Inputs { get; set; }
        }

        public IEnumerable<SuspendedTask> Tasks { get; set; }
    }

    public class CompleteResult : ProcessResult
    {
        public IDictionary<string, object> Output { get; set; }
    }

    public class Dependency
    {
        public string Name { get; set; }
        public string LocalName { get; set; }
        public string UiHint { get; set; }
    }
}
