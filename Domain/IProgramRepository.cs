﻿namespace Domain
{
    public interface IProgramRepository
    {
        void Save(string name, IProgram program);
        IProgram Get(string name);
        string[] ListPrograms();
        string[] ListPrograms(string outputType);
    }
}
