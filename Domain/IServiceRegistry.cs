﻿using System.Collections.Generic;

namespace Domain
{
    public interface IServiceRegistry
    {
        void RegisterService(string name, object service);
        IDictionary<string, object> GetServices(string[] names);
    }
}
