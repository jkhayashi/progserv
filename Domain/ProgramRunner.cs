﻿using System.Collections.Generic;
using System.Linq;

namespace Domain
{
    public class ProgramRunner
    {
        private readonly IServiceRegistry _serviceRegistry;
        private readonly IProgramRepository _programRepository;
        private readonly IProcessRepository _processRepository;
        private readonly IProcessIdGenerator _processIdGenerator;

        public ProgramRunner(IServiceRegistry serviceRegistry, IProgramRepository programRepository,
            IProcessRepository processRepository, IProcessIdGenerator processIdGenerator)
        {
            _serviceRegistry = serviceRegistry;
            _programRepository = programRepository;
            _processRepository = processRepository;
            _processIdGenerator = processIdGenerator;
        }

        public ProcessResult Start(string programName)
        {
            var program = _programRepository.Get(programName);
            var processId = _processIdGenerator.GenerateId();
            var process = new Process(processId, program);
            var result = process.Resume(_serviceRegistry);

            result.ProcessId = processId;

            _processRepository.Save(processId, process);

            return result;
        }

        public ProcessResult Resume(string processId, IDictionary<string, object> inputs)
        {
            var process = _processRepository.Get(processId);

            IProgram resumeWithProgram = null;
            var runProgramRequest = inputs.SingleOrDefault(i => i.Value is RunProgramRequest).Value as RunProgramRequest;

            if (runProgramRequest != null)
            {
                inputs.Remove(inputs.SingleOrDefault(i => i.Value is RunProgramRequest).Key);
                resumeWithProgram = _programRepository.Get(runProgramRequest.ProgramName);
            }

            var result = process.Resume(_serviceRegistry, inputs, withProgram: resumeWithProgram);

            result.ProcessId = process.ProcessId;

            _processRepository.Save(processId, process);

            if (result is CompleteResult)
                _processRepository.Delete(processId);

            return result;
        }
    }

    public class RunProgramRequest
    {
        public string ProgramName { get; set; }
    }
}
