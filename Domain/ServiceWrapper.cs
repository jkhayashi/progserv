﻿namespace Domain
{
    public class ServiceWrapper
    {
        public object Service { get; private set; }

        public ServiceWrapper(object service)
        {
            Service = service;
        }

        public void Invoke(string method, params object[] args)
        {
            Service.GetType().GetMethod(method).Invoke(Service, args);
        }

        public T Invoke<T>(string method, params object[] args)
        {
            return (T)Service.GetType().GetMethod(method).Invoke(Service, args);
        }
    }
}
