﻿namespace progserv.Impl.Data
{
    public class Customer
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
