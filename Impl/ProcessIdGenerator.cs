﻿using System;
using Domain;

namespace progserv.Impl
{
    public class ProcessIdGenerator : IProcessIdGenerator
    {
        public string GenerateId()
        {
            return Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Replace("/", "_").Replace("+", "-").Substring(0, 22);
        }
    }
}
