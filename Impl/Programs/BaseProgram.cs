﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;

namespace progserv.Impl.Programs
{
    public abstract class BaseProgram : IProgram
    {
        protected dynamic Heap { get; private set; }

        protected IDictionary<string, ServiceWrapper> Services { get; private set; }

        private Dictionary<string, Func<IDictionary<string, object>, ProcessResult>> _stateLookup;

        public Dependency[] Dependencies { get; protected set; }
        public Output[] Outputs { get; protected set; }

        public void Init(dynamic heap, IDictionary<string, object> dependencies)
        {
            Heap = heap;

            Services = dependencies
                .Select(kv => new KeyValuePair<string, ServiceWrapper>(kv.Key, new ServiceWrapper(kv.Value)))
                .ToDictionary(kv => kv.Key, kv => kv.Value);
        }

        protected void InitStates(Dictionary<string, Func<IDictionary<string, object>, ProcessResult>> stateLookup)
        {
            if (!stateLookup.ContainsKey("init"))
                throw new Exception("stateLookup missing required key 'init'");

            _stateLookup = stateLookup;
        }

        public ProcessResult Execute(IDictionary<string, object> inputs)
        {
            // TODO: This only works due to Process using ExpandoObject. It won't work for other dynamic types.
            if (!((IDictionary<string, object>)Heap).ContainsKey("state"))
                Heap.state = "init";

            return _stateLookup[Heap.state](inputs);
        }

        public void Abort()
        {
            throw new NotImplementedException();
        }

        protected void Invoke(string name, string method, params object[] args)
        {
            Services[name].Invoke(method, args);
        }

        protected T Invoke<T>(string name, string method, params object[] args)
        {
            return Services[name].Invoke<T>(method, args);
        }
    }
}
