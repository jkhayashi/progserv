﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using progserv.Impl.Data;

namespace progserv.Impl.Programs
{
    public class FindPatientProgram : BaseProgram
    {
        public FindPatientProgram()
        {
            InitStates(new Dictionary<string, Func<IDictionary<string, object>, ProcessResult>>
            {
                {"init", GetSearchParams},
                {"find_patient", FindPatient}
            });

            Dependencies = new[]
            {
                new Dependency { Name = "sys.customer" },
                new Dependency { Name = "sys.db" }
            };

            Outputs = new[] { new Output { Name = "patient" } };
        }

        private ProcessResult GetSearchParams(IDictionary<string, object> inputs)
        {
            Heap.state = "find_patient";
            
            return new SuspendedResult
            {
                Tasks = new[]
                {
                    new SuspendedResult.SuspendedTask
                    {
                        Name = "Find Patient",
                        Inputs = new[]
                        {
                            new Input
                            {
                                Name = "first_name",
                                Label = "First name",
                                Type = "text"
                            },
                            new Input
                            {
                                Name = "last_name",
                                Label = "Last name",
                                Type = "text"
                            },
                            new Input
                            {
                                Name = "dob",
                                Label = "Date of birth",
                                Type = "date"
                            }
                        }
                    }
                }
            };
        }

        private ProcessResult FindPatient(IDictionary<string, object> inputs)
        {
            var output = new Dictionary<string, object>();

            var customer = Services["sys.customer"].Service as Customer;

            var firstName = (string)inputs["first_name"];
            var lastName = (string)inputs["last_name"];
            var dob = (DateTime)inputs["dob"];

            var matches = Services["sys.db"].Invoke<object[]>("Find", "patient", new Dictionary<string, object>
            {
                {"FirstName", firstName},
                {"LastName", lastName},
                {"Dob", dob}
            });

            var patient = matches.FirstOrDefault() as Patient;

            // Also verify customer has access to patient
            if (patient != null)
            {
                if (!Services["sys.db"].Invoke<bool>("HasRelation", "sys.customer", customer.Id, "has-a", "patient", patient.Id))
                    throw new Exception(string.Format("Customer {0} does not have access to patient {1}", customer.Id, patient.Id));

                output.Add("patient", patient);
            }

            return new CompleteResult
            {
                Output = output
            };
        }
    }
}
