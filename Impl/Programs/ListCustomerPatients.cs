﻿using System;
using System.Collections.Generic;
using Domain;
using progserv.Impl.Data;

namespace progserv.Impl.Programs
{
    /// <summary>
    /// This class demonstrates how relationships can be queried.
    /// </summary>
    public class ListCustomerPatients : BaseProgram
    {
        public ListCustomerPatients()
        {
            InitStates(new Dictionary<string, Func<IDictionary<string, object>, ProcessResult>>
            {
                { "init", GetPatients}
            });
            Dependencies = new[]
            {
                new Dependency { Name = "sys.customer" },
                new Dependency { Name = "sys.db" }
            };
            Outputs = new[]
            {
                new Output {Name = "patients"}
            };
        }

        private ProcessResult GetPatients(IDictionary<string, object> arg)
        {
            var customerId = ((Customer) Services["sys.customer"].Service).Id;

            var patients = Services["sys.db"].Invoke<object[]>("GetRelated", "sys.customer", customerId, "has-a", "patient");

            return new CompleteResult
            {
                Output = new Dictionary<string, object>
                {
                    { "patients", patients }
                }
            };
        }
    }
}
