﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using progserv.Impl.Data;

namespace progserv.Impl.Programs
{
    public class PatientAssessmentProgram : BaseProgram
    {
        public PatientAssessmentProgram()
        {
            InitStates(new Dictionary<string, Func<IDictionary<string, object>, ProcessResult>>
            {
                {"init", GetPatient},
                {"get-assessments", GetAssessmentData},
                {"save-assessments", SaveAssessmentData}
            });

            Dependencies = new Dependency[0];

            Outputs = new[] { new Output { Name = "assessment" } };
        }


        private SuspendedResult GetPatient(IDictionary<string, object> inputs)
        {
            Heap.state = "get-assessments";

            return new SuspendedResult
            {
                Tasks = new[]
                {
                    new SuspendedResult.SuspendedTask
                    {
                        Inputs = new[]
                        {
                            new Input
                            {
                                Name = "patient",
                                Type = "patient"
                            }
                        }
                    }
                }
            };
        }

        private ProcessResult GetAssessmentData(IDictionary<string, object> inputs)
        {
            if (!inputs.ContainsKey("patient"))
            {
                var response = GetPatient(null);
                response.Tasks.First().Errors = new[] {"Patient needed!"};
                return response;
            }

            var patient = ((Patient)inputs["patient"]);

            Heap.state = "save-assessments";
            Heap.patient_id = patient.Id;

            Console.WriteLine("Starting assessment for {0} {1} ({2})", patient.FirstName, patient.LastName,
                patient.Dob.ToShortDateString());

            var yesNo = new[]
            {
                new Choice {Label = "Yes", Value = 1},
                new Choice {Label = "No", Value = 0}
            };

            return new SuspendedResult
            {
                Tasks = new[]
                {
                    new SuspendedResult.SuspendedTask
                    {
                        Name = "Gain-SS",
                        Inputs = new[]
                        {
                            new ChoiceInput
                            {
                                Label = "During the past 12 months, have you had significant problems with feeling very trapped, lonely, sad, blue, depressed, or hopeless about the future?",
                                Name = "gainss.question_1",
                                Choices = yesNo
                            },
                            new ChoiceInput
                            {
                                Label = "During the past 12 months, have you had significant problems with sleep trouble, such as bad dreams, sleeping restlessly or falling asleep during the day?",
                                Name = "gainss.question_2",
                                Choices = yesNo
                            },
                            new ChoiceInput
                            {
                                Label = "During the past 12 months, have you had significant problems with feeling very anxious, nervous, tense, scared, and panicked or like something bad was going to happen?",
                                Name = "gainss.question_3",
                                Choices = yesNo
                            },
                        }
                    }
                }
            };
        }

        private ProcessResult SaveAssessmentData(IDictionary<string, object> inputs)
        {
            var gainssScore = inputs
                .Where(i => i.Key.StartsWith("gainss."))
                .Select(i => Convert.ToInt32(i.Value))
                .Sum();
            
            //Services["db"].Invoke()

            return new CompleteResult
            {
                Output = new Dictionary<string, object>
                {
                    { "gain-ss", gainssScore }
                }
            };
        }
    }
}
