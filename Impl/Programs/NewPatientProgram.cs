﻿using System;
using System.Collections.Generic;
using Domain;
using progserv.Impl.Data;

namespace progserv.Impl.Programs
{
    public class NewPatientProgram : BaseProgram
    {

        public NewPatientProgram()
        {
            InitStates(new Dictionary<string, Func<IDictionary<string, object>, ProcessResult>>
            {
                {"init", GetPatientDemographics},
                {"save_patient", SavePatient}
            });

            Dependencies = new[]
            {
                new Dependency {Name = "sys.customer"},
                new Dependency {Name = "sys.person_id_gen"},
                new Dependency {Name = "sys.db"}
            };

            Outputs = new[] { new Output { Name = "patient" } };
        }

        private ProcessResult GetPatientDemographics(IDictionary<string, object> inputs)
        {
            Heap.state = "save_patient";

            return new SuspendedResult
            {
                Tasks = new[]
                {
                    new SuspendedResult.SuspendedTask
                    {
                        Inputs = new[]
                        {
                            new Input
                            {
                                Name = "first_name",
                                Label = "First name",
                                Type = "text"
                            },
                            new Input
                            {
                                Name = "last_name",
                                Label = "Last name",
                                Type = "text"
                            },
                            new Input
                            {
                                Name = "dob",
                                Label = "Date of birth",
                                Type = "date"
                            }
                        }
                    }
                }
            };
        }

        private ProcessResult SavePatient(IDictionary<string, object> inputs)
        {
            var customer = Services["sys.customer"].Service as Customer;

            var patientId = Services["sys.person_id_gen"].Invoke<string>("ReserveId");

            CompleteResult result;

            try
            {
                var patient = new Patient
                {
                    Id = patientId,
                    FirstName = (string)inputs["first_name"],
                    LastName = (string)inputs["last_name"],
                    Dob = (DateTime)inputs["dob"]
                };

                Invoke("sys.db", "Save", "patient", patientId, patient);
                Invoke("sys.db", "SaveRelation", "sys.customer", customer.Id, new[] { "has-a" }, "patient", patient.Id);

                result = new CompleteResult
                {
                    Output = new Dictionary<string, object>
                    {
                        {
                            "patient", patient
                        }
                    }
                };
            }
            catch (Exception)
            {
                Invoke("sys.person_id_gen", "Relinquish", patientId);
                throw;
            }

            return result;
        }
    }
}
