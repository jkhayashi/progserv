﻿using System.Collections.Generic;
using System.Linq;
using Domain;

namespace progserv.Impl.Repos
{
    public class InMemoryObjectRepository : IObjectRepository
    {
        private readonly ILogger _logger;
        
        // map from type -> key -> object
        // e.g., patient -> 123 -> { "name": "Bob" }
        private readonly Dictionary<string, Dictionary<string, object>> _objects =
            new Dictionary<string, Dictionary<string, object>>();

        // map from from_type -> from_key -> relation -> to_type -> to_key
        // e.g., customer 123 has-a patient 456,
        //       customer -> 123 -> has-a -> patient -> 456
        private readonly Dictionary<string, Dictionary<string, Dictionary<string, Dictionary<string, HashSet<string>>>>>
            _relations = new Dictionary<string, Dictionary<string, Dictionary<string, Dictionary<string, HashSet<string>>>>>();

        public InMemoryObjectRepository(ILogger logger)
        {
            _logger = logger;
        }

        public void Save(string type, string key, object obj)
        {
            if (!_objects.ContainsKey(type))
                _objects.Add(type, new Dictionary<string, object>());

            _objects[type][key] = obj;

            _logger.Log(LogLevel.Trace, "Saved object {0}:{1}\n{2}", type, key, obj);
        }

        public void SaveRelation(string fromType, string fromKey, IEnumerable<string> relations, string toType,
            string toKey)
        {
            if (!_relations.ContainsKey(fromType))
                _relations.Add(fromType, new Dictionary<string, Dictionary<string, Dictionary<string, HashSet<string>>>>());

            if (!_relations[fromType].ContainsKey(fromKey))
                _relations[fromType].Add(fromKey, new Dictionary<string, Dictionary<string, HashSet<string>>>());

            foreach (var relation in relations)
            {
                if (!_relations[fromType][fromKey].ContainsKey(relation))
                    _relations[fromType][fromKey].Add(relation, new Dictionary<string, HashSet<string>>());

                if (!_relations[fromType][fromKey][relation].ContainsKey(toType))
                    _relations[fromType][fromKey][relation].Add(toType, new HashSet<string>());

                _relations[fromType][fromKey][relation][toType].Add(toKey);
            }

            _logger.Log(LogLevel.Trace, "Saved relations ({4}) from {0}:{1} to {2}:{3}", fromType, fromKey, toType, toKey, string.Join(",", relations));
        }

        public bool HasRelation(string fromType, string fromKey, string relation, string toType, string toKey)
        {
            try
            {
                return _relations[fromType][fromKey][relation][toType].Contains(toKey);
            }
            catch (KeyNotFoundException) { }
            return false;
        }

        public object[] GetRelated(string fromType, string fromKey, string relation, string toType)
        {
            try
            {
                return _relations[fromType][fromKey][relation][toType]
                    .Select(objKey => Get(toType, objKey))
                    .ToArray();
            }
            catch (KeyNotFoundException) { }
            return new object[0];
        }

        public object Get(string type, string key)
        {
            return _objects[type][key];
        }

        public object[] Find(string type, IDictionary<string, object> properties)
        {
            if (!_objects.ContainsKey(type))
            {
                return new object[0];
            }

            return _objects[type]
                .Select(kv => kv.Value)
                .Where(obj => HasMatchingProperties(obj, properties)).ToArray();
        }

        private bool HasMatchingProperties(object obj, IDictionary<string, object> properties)
        {
            var candidateProperties = obj.GetType().GetProperties();

            foreach (var property in properties)
            {
                var candidateProp = candidateProperties.FirstOrDefault(p => p.Name == property.Key);
                
                if (candidateProp == null || !property.Value.Equals(candidateProp.GetValue(obj)))
                    return false;
            }

            return true;
        }
    }
}
