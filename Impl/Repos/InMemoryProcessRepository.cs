﻿using System.Collections.Generic;
using Domain;

namespace progserv.Impl.Repos
{
    public class InMemoryProcessRepository : IProcessRepository
    {
        private readonly Dictionary<string, Process> _processes = new Dictionary<string, Process>();

        public Process Get(string processId)
        {
            return _processes[processId];
        }

        public void Save(string processId, Process process)
        {
            _processes[processId] = process;
        }

        public void Delete(string processId)
        {
            _processes.Remove(processId);
        }
    }
}
