﻿using System.Collections.Generic;
using System.Linq;
using Domain;

namespace progserv.Impl.Repos
{
    public class InMemoryProgramRepository : IProgramRepository
    {
        private readonly Dictionary<string, IProgram> _programs = new Dictionary<string, IProgram>();

        public void Save(string name, IProgram program)
        {
            _programs[name] = program;
        }

        public IProgram Get(string name)
        {
            return _programs[name];
        }

        public string[] ListPrograms()
        {
            return _programs
                .Select(p => p.Key)
                .OrderBy(n => n).ToArray();
        }

        public string[] ListPrograms(string outputType)
        {
            return _programs
                .Where(p => p.Value.Outputs.Any(o => o.Name == outputType))
                .Select(p => p.Key)
                .OrderBy(n => n).ToArray();
        }
    }
}
