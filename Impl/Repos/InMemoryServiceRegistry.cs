﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;

namespace progserv.Impl.Repos
{
    public class InMemoryServiceRegistry : IServiceRegistry
    {
        private readonly Dictionary<string, object> _services = new Dictionary<string, object>();

        public void RegisterService(string name, object service)
        {
            if (_services.ContainsKey(name))
                throw new InvalidOperationException(string.Format("Service {0} already registered", name));
            _services[name] = service;
        }

        public IDictionary<string, object> GetServices(string[] names)
        {
            return names.ToDictionary(name => name, name => _services[name]);
        }
    }
}
