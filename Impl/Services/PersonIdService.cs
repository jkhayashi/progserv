﻿using System;
using System.Threading;

namespace progserv.Impl.Services
{
    public interface IPatientIdService
    {
        string ReserveId();
        void Relinquish(string id);
    }

    public class PersonIdService : IPatientIdService
    {
        private static int _seed = Environment.TickCount;
        private static readonly ThreadLocal<Random> RandomWrapper = new ThreadLocal<Random>(() =>
            new Random(Interlocked.Increment(ref _seed)));

        public string ReserveId()
        {
            const string baseJamesAlphabet = "0123456789BCDFGHJKMNPQRTVWXYZ";
            var buffer = new char[8];
            for (int i = 0; i < buffer.Length; ++i)
            {
                buffer[i] = baseJamesAlphabet[RandomWrapper.Value.Next(baseJamesAlphabet.Length - 1)];
            }
            return new string(buffer).Insert(6, "-").Insert(3, "-");
        }

        public void Relinquish(string id)
        {
            Console.WriteLine("Relinquished id " + id);
        }
    }
}
