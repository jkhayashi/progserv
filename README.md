## Introduction ##

This is a POC for a proposal to introduce the concept of a *program* into our architecture. 
 
Pushing complexity out of our core architecture and into programs allows us to focus on building good, well-defined interactions between users and the system, and between components in the system.

## Overview ##
In computing we have:

1. *programs*, or instructions
1. *processes*, or instances of programs

Interaction with a program is defined by its input and output. This simple input/output definition allows us to do interesting things like chain programs together without foreknowledge of how they should be chained.

Programs may also specify dependencies. Dependencies allow programs to do external things like produce side effects (e.g., write to a database) or query a third-party service.

Briefly, here’s how the system works:

1. A program is started, returning a process Id
1. The process runs until it either *suspends* (i.e., asks for input) or *completes*
1. If the process suspends, the user provides the process the input it needs to resume
1. Steps 2 and 3 are performed until the process completes

### Some benefits ###

- **A program is as flexibile as its language**, so there are no dead ends to worry about. Not to mention that programs can be written in any language we choose to hook into the platform. For instance, we can use C# for treatment plan, while using lua, boo, or even javascript for programs built with the UI.
- **Workflow builders can still be used**. We would perform a one-way translation from a workflow builder tree to program.
- **EAV can still be used**. But instead of being "the" data store, it becomes an optional dependency.
- **Plugins are straightforward to implement**. Once a plugin is registered as a service, programs only need to specify a dependency to use it.
- **Building components is faster**. You need to know less about the system.
- **HTTP API becomes simpler**, reflecting the back-and-forth nature of interaction with a program.

### Notes ###

- A *program* is analogous to a *workflow version*
- A *process* is analogous to a *workflow*
- A *suspended process* (a process that can be resumed) is analogous to a *user task* and a *form*